#!/usr/bin/python3

"""
scan a music directory and output a list of artists
"""

import os

import mutagen


def scan_dir(directory):
    """Scan a directory and output a list of artists"""
    raw_artists = []
    for dirname, _, filenames in os.walk(directory, topdown=False):
        for song in filenames:
            if song.endswith(('.flac', '.mp3', '.ogg')):
                try:
                    artist = mutagen.File(os.path.join(dirname, song))["artist"]
                    raw_artists.append(''.join(artist))  # list to string
                except:  # pylint: disable=W0702
                    pass
    return set(raw_artists)
