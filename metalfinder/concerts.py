#!/usr/bin/python3

"""
get concerts data from API provider
"""

import pprint

from datetime import date

from .api.bandsintown import Client


def query_bit(raw_artists, args):
    """Query Bandsintown for concert list"""
    bit_client = Client(args.bit_appid)
    for artist in raw_artists:
        if args.max_date:
            date_range = str(date.today()) + ',' + args.max_date
            raw_concerts = bit_client.artists_events(artist, date=date_range)
        else:
            raw_concerts = bit_client.artists_events(artist)
    return raw_concerts


def parse_bit(raw_concerts):
    """Parse and format raw data from Bandsintown"""
#    concert_dict = {}
    for concert in raw_concerts:
        pprint.pprint(concert)
#    return concert_dict


def bit(raw_artists, args):
    """Wrapper function for Bandsintown provider"""
    raw_concerts = query_bit(raw_artists, args)
    parse_bit(raw_concerts)
